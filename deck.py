"""Deck."""
import requests
import random


class Card:
    """Simple dataclass for holding card information."""

    def __init__(self, value: str, suit: str, code: str, top_down=False):
        """Constructor."""
        self.value = value
        self.suit = suit
        self.code = code
        self.top_down = top_down

    def __str__(self):
        """Str."""
        if self.top_down is False:
            return f"{self.code}"
        else:
            return "??"

    def __repr__(self) -> str:
        """Repr."""
        return f"{self.code}"

    def __eq__(self, o) -> bool:
        """Eq."""
        if isinstance(o, Card):
            if self.value == o.value and self.suit == o.suit:
                return True
        else:
            return False


class Deck:
    """Deckk."""

    DECK_BASE_API = "https://deckofcardsapi.com/api/deck/"
    codes = ['AS', '2S', '3S', '4S', '5S', '6S', '7S', '8S', '9S', '0S', 'JS', 'QS', 'KS',
             'AD', '2D', '3D', '4D', '5D', '6D', '7D', '8D', '9D', '0D', 'JD', 'QD', 'KD',
             'AC', '2C', '3C', '4C', '5C', '6C', '7C', '8C', '9C', '0C', 'JC', 'QC', 'KC',
             'AH', '2H', '3H', '4H', '5H', '6H', '7H', '8H', '9H', '0H', 'JH', 'QH', 'KH']
    values = ['ACE', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'JACK', 'QUEEN', 'KING']
    suits = ['SPADES', 'DIAMONDS', 'CLUBS', 'HEARTS']

    def __init__(self, deck_count: int = 1, shuffle: bool = False):
        """Constructor."""
        if shuffle:
            response = requests.get(Deck.DECK_BASE_API + f"new/shuffle/?deck_count={deck_count}").json()
        else:
            response = requests.get(Deck.DECK_BASE_API + f"new/?deck_count={deck_count}").json()
        self.deck_id = response.get("deck_id")

        self.deck_count = deck_count
        self.is_shuffled = shuffle
        self.remaining = 52 * deck_count
        self._backup_deck = self._generate_backup_pile(self.is_shuffled, self.deck_count)

    def shuffle(self) -> None:
        """Shuffle the deck."""
        requests.get(f"https://deckofcardsapi.com/api/deck/{self.deck_id}/shuffle/").json()
        self._backup_deck = random.shuffle(self._backup_deck)
        self.is_shuffled = True

    def draw_card(self, top_down: bool = False):
        """
        Draw card from the deck.

        :return: card instance.
        """
        response = requests.get(f"{Deck.DECK_BASE_API}{self.deck_id}/draw/?count=1")
        if response.status_code == requests.codes.ok:
            i = response.json()
            if i.get("success", False) is True:
                if self.remaining > 0:
                    card = i["cards"][0]
                    new_card = Card(card.get("value"), card.get("suit"), card.get("code"), top_down)
                    if new_card in self._backup_deck:
                        self._backup_deck.remove(new_card)
                        self.remaining -= 1
                    return new_card
        else:
            if self.remaining > 0:
                card = self._backup_deck[0]
                self._backup_deck.remove(card)
                self.remaining -= 1
                return card

    def _request(self, url: str) -> dict:
        """Update deck."""
        response = requests.get(url).json()
        if response.get("success") is True:
            self.deck_id = response.get("deck_id")
            self.is_shuffled = response.get("shuffled")
            self.remaining = response.get("remaining")
        return response

    @staticmethod
    def _generate_backup_pile(is_shuffled, deck_count):
        """Generate backup pile."""
        values = ['ACE', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'JACK', 'QUEEN', 'KING']
        suits = ['SPADES', 'DIAMONDS', 'CLUBS', 'HEARTS']
        backup_deck = []
        if not is_shuffled:
            for i in suits:
                for e in values:
                    value = e
                    suit = i
                    code = e[0] + i[0]
                    backup_deck.append(Card(value, suit, code))
        else:
            for i in suits:
                for e in values:
                    value = e
                    suit = i
                    code = e[0] + i[0]
                    backup_deck.append(Card(value, suit, code))
                    random.shuffle(backup_deck)
        return backup_deck * deck_count


if __name__ == '__main__':

    d = Deck(shuffle=True)
    print(d.remaining)  # 52
    card1 = d.draw_card()  # Random card
    print(card1 in d._backup_deck)  # False
    print(d._backup_deck)  # 51 shuffled cards
    print(d._backup_deck[0].value, d._backup_deck[0].suit)
    d2 = Deck(deck_count=2)
    print(d2._backup_deck)  # 104 ordered cards (deck after deck)
    print(d.draw_card())
